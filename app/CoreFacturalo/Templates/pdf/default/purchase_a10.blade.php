@php
    $establishment = $document->establishment;
    
    $tittle = $document->series.'-'.str_pad($document->number, 8, '0', STR_PAD_LEFT);
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
<table class="full-width">
    <tr>
        @if($company->logo)
            <td width="20%">
                <div class="company_logo_box">
                    <img src="data:{{mime_content_type(public_path("storage/uploads/logos/{$company->logo}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/logos/{$company->logo}")))}}" alt="{{$company->name}}" class="company_logo" style="max-width: 150px;">
                </div>
            </td>
        @else
            <td width="20%">
                {{--<img src="{{ asset('logo/logo.jpg') }}" class="company_logo" style="max-width: 150px">--}}
            </td>
        @endif
        <td width="50%" class="pl-3">
            <div class="text-left">
                <h4 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
               

            </div>
        </td>
        <td width="20%" class="pl-3">
            <div class="text-left">
            <h3 class="text-center">Numero : 0000{{ $document->id}}</h3>
               

            </div>
        </td>
        
    </tr>
</table>
<table class="full-width" border="1">
    <tr>      
      
        <td width="50%" class="pl-3">
       <b> 9.PLACA CARRETA : </b>{{ $document->placa}}
        </td>
        <td width="50%" class="pl-3">
        <b>FECHA: </b> {{ $document->fecha}}
        </td>
        
    </tr>
    <tr>      
      
    
      <td width="50%" class="pl-3">
      <b>RESPONSABLE :</b> {{ $document->responsable}}
      </td>
      
  </tr>
   
</table>

<hr>
<table class="full-width" border="1">
    <tr>      
      
        <td width="30%" class="pl-3">
        <b> 10. DOCUMENTACION CARRETA  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        <td width="30%" class="pl-3">
        <b> 11. ARTICULOS EN TRACTO  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        
    </tr>
    <tr>      
      
        <td width="30%" class="pl-3">
        Tarjeta de Propiedad  
        </td>
        <td width="5%" class="pl-3">
        
        @if ($document->tarjetap == '1')
            <span>X</span>    
         @endif
         
        </td>
        <td width="5%" class="pl-3">
        @if ($document->tarjetap == '2')
            <span>X</span>    
         @endif
        </td>
        <td width="30%" class="pl-3">
        Alarma Retroceso  
        </td>
        <td width="5%" class="pl-3">
        @if ($document->alarma == '1')
            <span>X</span>    
         @endif
        </td>
        <td width="5%" class="pl-3">
        @if ($document->alarma == '2')
            <span>X</span>    
         @endif 
        </td>
        
    </tr>
    <tr>      
      
      <td width="30%" class="pl-3">
      Tarjeta de Circulacion 
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->tarjetac == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->tarjetac == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Conos
      </td>
      <td width="5%" class="pl-3">
      @if ($document->conos == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->conos == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      SOAT
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->soat == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->soat == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Tacos
      </td>
      <td width="5%" class="pl-3">
      @if ($document->tacos == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->tacos == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Revision Tecnica
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->revisiont == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->revisiont == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      toldera
      </td>
      <td width="5%" class="pl-3">
      @if ($document->toldera == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->toldera == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Bonificacion
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->bonificacion == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->bonificacion == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Plastico
      </td>
      <td width="5%" class="pl-3">
      @if ($document->plastico == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->plastico == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Soga
      </td>
      <td width="5%" class="pl-3">
      @if ($document->soga == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->soga == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Malla
      </td>
      <td width="5%" class="pl-3">
      @if ($document->malla == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->malla == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Neumatico de Repuesto
      </td>
      <td width="5%" class="pl-3">
      @if ($document->neumatico == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->neumatico == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Palos cortos
      </td>
      <td width="5%" class="pl-3">
      @if ($document->palosc == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->palosc == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Palos altos
      </td>
      <td width="5%" class="pl-3">
      @if ($document->palosa == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->palosa == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Latas
      </td>
      <td width="5%" class="pl-3">
      @if ($document->latas == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->latas == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Extintor tracto
      </td>
      <td width="5%" class="pl-3">
      @if ($document->extintort == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->extintort == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Extintor carreta
      </td>
      <td width="5%" class="pl-3">
      @if ($document->extintorc == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->extintorc == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Fajas
      </td>
      <td width="5%" class="pl-3">
      @if ($document->fajas == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->fajas == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Ratchet
      </td>
      <td width="5%" class="pl-3">
      @if ($document->ratchet == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->ratchet == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
    
      </td>
      <td width="5%" class="pl-3">
      
     
       
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="30%" class="pl-3">
      Gancho
      </td>
      <td width="5%" class="pl-3">
      @if ($document->gancho == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->gancho == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>      
      
        <td width="30%" class="pl-3">
        <b> 12. EQUIPO  </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        <td width="30%" class="pl-3">
        <b> 13. NEUMATICOS </b>
        </td>
        <td width="5%" class="pl-3">
        <b>SI </b> 
        </td>
        <td width="5%" class="pl-3">
        <b>NO </b> 
        </td>
        
    </tr>
    <tr>      
      
        <td width="30%" class="pl-3">
        Porta llanta
        </td>
        <td width="5%" class="pl-3">
        
        @if ($document->portallanta == '1')
            <span>X</span>    
         @endif
         
        </td>
        <td width="5%" class="pl-3">
        @if ($document->portallanta == '2')
            <span>X</span>    
         @endif
        </td>
        <td width="30%" class="pl-3">
        Vigia en buene estado  
        </td>
        <td width="5%" class="pl-3">
        @if ($document->vigia == '1')
            <span>X</span>    
         @endif
        </td>
        <td width="5%" class="pl-3">
        @if ($document->vigia == '2')
            <span>X</span>    
         @endif 
        </td>
        
    </tr>
    <tr>      
      
      <td width="30%" class="pl-3">
      Porta palos
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->portapalos == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->portapalos == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      Perdida de aire
      </td>
      <td width="5%" class="pl-3">
      @if ($document->aire == '1')
          <span>X</span>    
       @endif
      </td>
      <td width="5%" class="pl-3">
      @if ($document->aire == '2')
          <span>X</span>    
       @endif 
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces lado derecho
      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesd == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesd == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces lado izquierdo

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesi == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesi == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces posteriores



      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesp == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesp == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces Intermitentes

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesint == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesint == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>


  <tr>      
      
      <td width="30%" class="pl-3">
      Luces retroceso

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesre == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesre == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Luces de freno


      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->lucesfre == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->lucesfre == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
  <tr>      
      
      <td width="30%" class="pl-3">
      Neblineros

      </td>
      <td width="5%" class="pl-3">
      
      @if ($document->neblineros == '1')
          <span>X</span>    
       @endif
       
      </td>
      <td width="5%" class="pl-3">
      @if ($document->neblineros == '2')
          <span>X</span>    
       @endif
      </td>
      <td width="30%" class="pl-3">
      
      </td>
      <td width="5%" class="pl-3">
     
      </td>
      <td width="5%" class="pl-3">
      
      </td>
      
  </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>    
    
    <td width="20%" class="pl-3"> 
    <br>
   
    BUENO <b>B</b><br>
    REGULAR <b>R</b><br>
    MALO <b>M</b>
    </td>
    <td width="50%" class="pl-3">
    
    <b>NEUMATICOS</b><br>
    <table class="full-width" border="1">
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta1}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta5}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta9}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta2}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta6}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta10}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta3}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta7}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta11}}</td>
        </tr>
        <tr>    
        <td width="30%" class="pl-3">{{ $document->llanta4}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta8}}</td>
        <td width="30%" class="pl-3">{{ $document->llanta12}}</td>
        </tr>
    </table>
    </td>
    <td width="30%" class="pl-3">
    <span style="background-color: rgb(0, 13, 255);
    color: white;
    padding: 7px;
    border-radius: 12px;"> REPUESTO 01</span> : {{ $document->llanta13}} <br>
    <span style="background-color: rgb(0, 13, 255);
    color: white;
    padding: 7px;
    border-radius: 12px;"> REPUESTO 02</span> : {{ $document->llanta14}}
    </td>
    </tr>
</table>
<hr>
<table class="full-width" border="1">
    <tr>    
    <td width="100%" class="pl-3">
    Observaciones del Tracto: {{ $document->observaciones}}
    </td>
    </tr>
    </table>
    <hr>
    <table class="full-width" border="1">
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc1}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen1}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen1}")))}}" alt="{{$document->imagen1}}"  style="max-width: 500px;">
                </div>
    </td>       
    </tr> 
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc2}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen2}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen2}")))}}" alt="{{$document->imagen2}}"  style="max-width: 500px;">
                </div>
                </td>       
    </tr> 
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc3}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen3}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen3}")))}}" alt="{{$document->imagen3}}"  style="max-width: 500px;">
                </div>
                </td>       
    </tr> 
    <tr>
    <td width="100%" class="pl-3">
    <br>
    Descripción imagen : {{ $document->desc4}} <br>
    <br>
                <div >
                    <img src="data:{{mime_content_type(public_path("storage/uploads/items/{$document->imagen4}"))}};base64, {{base64_encode(file_get_contents(public_path("storage/uploads/items/{$document->imagen4}")))}}" alt="{{$document->imagen4}}"  style="max-width: 500px;">
                </div>
                </td>       
    </tr> 
    </table>           

</body>
</html>
