@php
    $establishment = $document->establishment;
    $supplier = $document->supplier;
    
    $tittle = $document->series.'-'.str_pad($document->number, 8, '0', STR_PAD_LEFT); 
@endphp
<html>
<head>
    {{--<title>{{ $tittle }}</title>--}}
    {{--<link href="{{ $path_style }}" rel="stylesheet" />--}}
</head>
<body>
<table class="full-width">
<tr>
       
        <td  class="pl-3" height="20">
       
            </td>
        
    </tr>
<tr>
       
        <td  class="pl-3" height="20">
        <h5 class="text-center"><b>{{ $document->document_type->description}}</b></h5>
            </td>
        
    </tr>
    <tr>
       
        <td  class="pl-3">
            <div class="text-left">
            
                <h5 class="">{{ $company->name }}</h4>
                <h5>{{ 'RUC '.$company->number }}</h5>
                <h5 class="text-center">NRO DE INGRESO : 1{{ $tittle }}</h6>
               
            </div>
        </td>
        
    </tr>
</table>
<table class="full-width mt-5">

<tr>
        <td width="15%">Cliente: {{ $document->customer}}</td>
        
      
    </tr>
    
    <tr>
       
        
        <td width="50%">Fecha de emisión:</td>
        <td width="40%">{{ $document->date_of_issue->format('Y-m-d') }}</td>
    </tr>
    
 
   
    <tr>
        <td class="align-top">Usuario:</td>
        <td colspan="3">
            {{ $document->user->name }}
        </td>
    </tr>
    @if($document->purchase_order)
    <tr>
        <td class="align-top">O. Compra:</td>
        <td  colspan="3">{{ $document->purchase_order->number_full }}</td>
    </tr>
    @endif
</table>
 

<table class="full-width mt-10 mb-10">
    <thead class="">
    <tr class="bg-grey">
       
        <th class="border-top-bottom text-left py-2" width="40%">DESCRIPCIÓN</th>
       
        <th class="border-top-bottom " width="30%">TOTAL</th>
    </tr>
    </thead>
    <tbody>
    @foreach($document->items as $row)
        <tr>
            
            
            <td width="60%">{{$row->description }}</td>
            
            <td  width="40%">{{ number_format($row->total, 2) }}</td>
        </tr>
        
    @endforeach
       
    </tbody>
</table>


<table class="full-width" style="margin-top:40px">
    <tr>
         
            <td colspan="6" class="border-bottom"></td>
        
    </tr>
    <tr>
         
            <td colspan="6" style="margin-left:20px"><b> FIRMA</b></td>
        
    </tr>
</table>
</body>
</html>
