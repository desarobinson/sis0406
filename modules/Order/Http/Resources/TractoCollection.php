<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TractoCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($row, $key) {
            return [
                'id' => $row->id,
                'conductor' => $row->conductor,
                'placa' => $row->placa,                
                'fecha' => $row->fecha,                
                'responsable' => $row->responsable,
                'articulos' => $row->articulos,
                'tarjetap' => $row->tarjetap,
                'tarjetac' => $row->tarjetac,
                'soat' => $row->soat,
                'revision' => $row->revision,
                'bonificacion' => $row->bonificacion,
                'formatos' => $row->formatos, 
                'alarma' => $row->alarma,  
                'botiquin' => $row->botiquin,  
                'elementos' => $row->elementos,                
                'gata' => $row->gata,  
                'llaves' => $row->llaves,  
                'autoradio' => $row->autoradio,  
                'nagua' => $row->nagua,  
                'naceite' => $row->naceite,  
                'luces' => $row->luces,  
                'frenos' => $row->frenos,  
                'embrague' => $row->embrague,  
                'aguaaceite' => $row->aguaaceite,  
                'tacografo' => $row->tacografo,  
                'vigia' => $row->vigia, 
                'aire' => $row->aire,  
                'llanta1' => $row->llanta1, 
                'llanta2' => $row->llanta2, 
                'llanta3' => $row->llanta3, 
                'llanta4' => $row->llanta4, 
                'llanta5' => $row->llanta5, 
                'llanta6' => $row->llanta6, 
                'llanta7' => $row->llanta7, 
                'llanta8' => $row->llanta8, 
                'llanta9' => $row->llanta9, 
                'llanta10' => $row->llanta10,
                'desc01' => $row->desc01,
                'desc02' => $row->desc02,
                'desc03' => $row->desc03,
                'desc04' => $row->desc04,
                'combustibleactual' => $row->combustibleactual,
                'combustiblentrega' => $row->combustiblentrega,
                'observaciones' => $row->observaciones,
                'km' => $row->km,
                'filename' => $row->filename,
                'external_id' => $row->external_id,
                'image_url' => ($row->imagen1 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen1) : asset("/logo/{$row->imagen1}"),
                'image_url1' => ($row->imagen2 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen2) : asset("/logo/{$row->imagen2}"),
                'image_url2' => ($row->imagen3 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen3) : asset("/logo/{$row->imagen3}"),
                'image_url3' => ($row->imagen4 !== 'imagen-no-disponible.jpg') ? asset('storage'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'items'.DIRECTORY_SEPARATOR.$row->imagen4) : asset("/logo/{$row->imagen4}"),
                'created_at' => $row->created_at->format('Y-m-d H:i:s'),
                'updated_at' => $row->updated_at->format('Y-m-d H:i:s'),
            ];
        });
    }
}
