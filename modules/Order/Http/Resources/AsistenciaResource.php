<?php

namespace Modules\Order\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RutaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,            
            'nombres' => $this->nombres,
            'id_usuario'=> $this->id_usuario,
            'fecha'=> $this->fecha,
            'hora'=> $this->hora,
            'comentario'=> $this->comentario,
            'tipo' => $this->tipo      
            

        ];
    }
}
